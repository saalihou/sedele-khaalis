unit CompteRepresentant;

interface
    uses sysutils, S_Types, Constantes, Representant;
    
    { Insere un compte de representant dans le fichier }
    { @param Le compte de representant a inserer }
    { @return le proprietaire du compte insere, sinon -1 }
    function inserer(compte_representant: S_Types.compte_representant): S_Types.operateur_representant;

    { Lit un compte de representant depuis un fichier }
    { @param l'identifiant de l'operateur chez qui le compte est ouvert }
    { @param le representant a qui est attirbue le compte }
    { @return le compte de representant lu }
    function lire(operateur, representant: integer): S_Types.compte_representant;

    { Solde un compte de representant }
    { @param l'identifiant de l'operateur chez qui le compte est ouvert }
    { @param le representant a qui est attribue le compte }
    { @param le montant a attribuer au solde du compte }    
    { @return vrai si le solde a ete attribue, faux sinon }
    function determiner_solde(operateur, representant: integer; montant: longint): boolean;

    { Supprime un compte de representant }
    { @param l'identifiant de l'operateur chez qui le compte est ouvert }
    { @param le representant a qui est attirbue le compte }
    { @return vrai si la suppression a reussi, faux sinon }
    function supprimer(operateur, representant: integer): boolean;

    { Verifie si le compte du representant appartenant a @proprietaire existe }
    { @param l'identifiant de l'operateur chez qui le compte est ouvert }
    { @param le representant a qui est attirbue le compte }
    { @return vrai si le compte du representant existe, faux sinon }
    function existe(operateur, representant: integer): boolean;

implementation
    
    { ================================= }
    function compter(): integer;
    var
        f_comptes_representants: THandle;
    begin
        f_comptes_representants := FileOpen(Constantes.fichier_comptes_representants, fmOpenRead);
        compter := FileSeek(f_comptes_representants, 0, fsFromEnd) div sizeOf(S_Types.compte_representant);
        FileClose(f_comptes_representants);
    end;

    { ================================= }
    function ecrire_position(compte_representant: S_Types.compte_representant; index: integer): boolean;
    var
        f_comptes_representants: THandle;
        nbComptes: integer;
    begin
        f_comptes_representants := FileOpen(Constantes.fichier_comptes_representants, fmOpenWrite);
        nbComptes := compter();
        FileSeek(f_comptes_representants, sizeOf(S_Types.compte_representant) * index, fsFromBeginning);
        
        if (FileWrite(f_comptes_representants, compte_representant, sizeOf(S_Types.compte_representant)) <> -1) then
            ecrire_position := false
        else
            ecrire_position := true;

        FileClose(f_comptes_representants);
    end;

    { ================================= }
    function lire_position(index: integer): S_Types.compte_representant;
    var
        f_comptes_representants: THandle;
        compte_representant: S_Types.compte_representant;
        nbComptes: integer;
    begin
        f_comptes_representants := FileOpen(Constantes.fichier_comptes_representants, fmOpenRead);
        nbComptes := compter();
        FileSeek(f_comptes_representants, sizeOf(S_Types.compte_representant) * index, fsFromBeginning);
        FileRead(f_comptes_representants, compte_representant, sizeOf(S_Types.compte_representant));
        lire_position := compte_representant;
        FileClose(f_comptes_representants);
    end;

    { ================================= }
    function chercher_position(operateur, representant: integer): integer;
    var
        compte_representant: S_Types.compte_representant;
        nbComptes, index: integer; 
    begin
        nbComptes := compter();
        chercher_position := -1;

        for index:=0 to nbComptes - 1 do
        begin
            compte_representant := lire_position(index);
            if ( (compte_representant.id.operateur = operateur) and (compte_representant.id.representant = representant) ) then
            begin
                chercher_position := index;
                exit;
            end;
        end;
    end;
    
    { ================================= }
    function inserer(compte_representant: S_Types.compte_representant): S_Types.operateur_representant;
    var
        dernier_compte_representant: S_Types.compte_representant;
        valeur_retour: S_Types.operateur_representant;
        nbComptes: integer;
    begin
        nbComptes := compter();
        ecrire_position(compte_representant, nbComptes);
        valeur_retour.operateur := compte_representant.id.operateur;
        valeur_retour.representant := compte_representant.id.representant;
        inserer := valeur_retour;
    end;

    { ================================= }
    function lire(operateur, representant: integer): S_Types.compte_representant;
    begin
        lire := lire_position(chercher_position(operateur, representant));
    end;

    { ================================= }
    function supprimer(operateur, representant: integer): boolean;
    var
        f_comptes_representants: THandle;
        compte_representant: S_Types.compte_representant;
        index, nbComptes: integer;
    begin
        nbComptes := compter();
        index := chercher_position(operateur, representant);

        if (index <> - 1) then
        begin
            for index:=index to nbComptes - 2 do
            begin
                compte_representant := lire_position(index + 1);
                ecrire_position(compte_representant, index);
            end;

            f_comptes_representants := FileOpen(Constantes.fichier_comptes_representants, fmOpenWrite);
            FileTruncate(f_comptes_representants, (nbComptes - 1) * sizeOf(S_Types.compte_representant));
            FileClose(f_comptes_representants);
        end;

        supprimer := true;
    end;

    { ================================= }
    function determiner_solde(operateur, representant: integer; montant: longint): boolean;
    var
        compte_representant: S_Types.compte_representant;
        index: integer;
    begin
        index := chercher_position(operateur, representant);
        compte_representant := lire_position(index);
        compte_representant.solde := montant;
        ecrire_position(compte_representant, index);
        determiner_solde := true;
    end;

    { ================================= }
    function existe(operateur, representant: integer): boolean;
    begin
        if (chercher_position(operateur, representant) = -1) then
            existe := false
        else
            existe := true;
    end;
end.