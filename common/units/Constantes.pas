unit Constantes;
{ Ensemble des constantes utilisees par l'api }
interface
	uses sysutils;

	var 
		{ FICHIERS }
		{ ================================= }
		chemin_api: string;
		chemin_fichiers_api: string;
		fichier_admin, fichier_clients, fichier_comptes_clients, fichier_operateurs, fichier_comptes_operateurs, 
		fichier_representants, fichier_comptes_representants, fichier_operations: string;


implementation

initialization
begin
	{ FICHIERS }
	{ ================================= }
	chemin_api := '/home/saalihou/Documents/work/dut1project'; // a editer individuellement par chaque eleve
	chemin_fichiers_api := chemin_api + '/common/fichiers';
	fichier_admin := chemin_fichiers_api + '/admin.dat';
	fichier_clients := chemin_fichiers_api + '/clients.dat';
	fichier_comptes_clients := chemin_fichiers_api + '/comptes_clients.dat';
	fichier_operateurs := chemin_fichiers_api + '/operateurs.dat';
	fichier_comptes_operateurs := chemin_fichiers_api + '/comptes_operateurs.dat';
	fichier_representants := chemin_fichiers_api + '/representants.dat';
	fichier_comptes_representants := chemin_fichiers_api + '/comptes_representants.dat';
	fichier_operations := chemin_fichiers_api + '/operations.dat';
end;

end.