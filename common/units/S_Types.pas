unit S_Types;

interface

{ ================================= }
type client = record
	id: integer;
	prenom: string[50];
	nom: string[50];
	numero: string[9];
	mot_de_passe: string[30];
end;

{ ================================= }
type compte_client = record
	proprietaire: integer; // reference vers un client
	solde: longint;
	credit_tel: integer;
end;

{ ================================= }
type service_operateur = (credit, facture, retrait, transfert);

type operateur = record
	id: integer;
	nom: string[50];
	nbreServices: integer;
	services: array [0..9] of service_operateur;
end;

{ ================================= }

{ ================================= }
type tableau_operateurs=array[0..999] of operateur;

type compte_operateur = record
	proprietaire: integer; // reference vers un operateur
	solde: longint;
end;

{ ================================= }
type representant = record
	id: integer;
	prenom: string[50];
	nom: string[50];
end;

{ ================================= }
type operateur_representant = record // Cle composee qui constitue l'identifiant d'un compte de representant
	operateur: integer;
	representant: integer
end;

type compte_representant = record
	id: operateur_representant;
	solde: longint;
end;

{ ================================= }
type type_operation = (achat_credit, paiement_facture, transfert_argent, creance_representant, debit_representant);

type dateheure = record
	jour: integer;
	mois: integer;
	annee: integer;
	heure: integer;
	minute: integer;
	secondes: integer;
end;

type operation = record
	id: integer;
	sorte: type_operation; // je l'ai appele sorte parce que "type" est un mot reserve
	compte_source: integer; // reference vers un compte client, representant ou operateur
	compte_destination: integer; // reference vers un compte client, representant ou operateur
	montant: integer;
	date: dateheure;
end;



implementation

end.