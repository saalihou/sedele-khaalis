program compte_operateur_test;

uses CompteOperateur, S_Types;

var
	compte_OrangeMoney: S_Types.compte_operateur;
	idOrangeMoney: integer;

begin
	writeln('Insertion du compte de OrangeMoney...');
	compte_OrangeMoney.proprietaire := 1;
	compte_OrangeMoney.solde := 0;

	idOrangeMoney := CompteOperateur.inserer(compte_OrangeMoney);

	writeln('Verification de l''existence du compte de OrangeMoney');
	writeln(CompteOperateur.existe(idOrangeMoney));

	writeln('Lecture du compte de OrangeMoney...');
	compte_OrangeMoney := CompteOperateur.lire(idOrangeMoney);
	writeln(compte_OrangeMoney.proprietaire);
	writeln(compte_OrangeMoney.solde);

	writeln('Attritbution de 100.000f au solde du compte OrangeMoney');
	CompteOperateur.determiner_solde(idOrangeMoney, 100000);

	writeln('Relecture du compte de OrangeMoney');
	compte_OrangeMoney := CompteOperateur.lire(idOrangeMoney);
	writeln(compte_OrangeMoney.proprietaire);
	writeln(compte_OrangeMoney.solde);

	writeln('Suppression du compte de OrangeMoney');
	CompteOperateur.supprimer(idOrangeMoney);

	writeln('Reverification de l''existence du compte de OrangeMoney');
	writeln(CompteOperateur.existe(idOrangeMoney));

end.