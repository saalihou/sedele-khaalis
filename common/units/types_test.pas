program test_types;

uses S_Types;
var
	modou: S_Types.client;
	compte_modou: S_Types.compte_client;
	{ ================================= }
	orange: S_Types.operateur;
	compte_orange: S_Types.compte_operateur;
	{ ================================= }
	ndongo: S_Types.representant;
	compte_ndongo_orange: S_Types.compte_representant;
	{ ================================= }
	transfert_modou: S_Types.operation;

	i: integer;

begin
	modou.id := 1;
	modou.prenom := 'Modou';
	modou.nom := 'Ndiaye';
	modou.numero := '771234567';
	writeln('Le client ' ,modou.prenom, ' ' ,modou.nom, ' d''identifiant ' ,modou.id, ' a pour numero ' ,modou.numero);
	writeln;

	{ ================================= }
	compte_modou.proprietaire := 1; // Modou
	compte_modou.solde := 10500;
	compte_modou.credit_tel := 700;
	writeln('Le compte appartent au client d''identifiant ' ,compte_modou.proprietaire, ' a comme solde ' ,compte_modou.solde, 
			' et comme montant de recharge telephonique ' ,compte_modou.credit_tel);
	writeln;

	{ ================================= }
	orange.id := 1;
	orange.nom := 'Orange';
	orange.services[1] := S_Types.transfert;
	orange.services[2] := S_Types.retrait;
	orange.services[3] := S_Types.credit;
	writeln('L''operateur ' ,orange.nom, ' d''identifiant ' ,orange.id, ' propose les services suivant: ');
	for i:=1 to 3 do
	begin
		case orange.services[i] of
			S_Types.transfert:
				writeln('Transfert d''argent');
			S_Types.retrait:
				writeln('Retrait d''argent');
			S_Types.facture:
				writeln('Facturation');
			S_Types.credit:
				writeln('Vente de credit');
		end;
	end;
	writeln;

	{ ================================= }
	compte_orange.proprietaire := 1; // Orange
	compte_orange.solde := 230000;
	writeln('Le compte de l''operateur d''identifiant ' ,compte_orange.proprietaire, ' a pour solde ' ,compte_orange.solde);
	writeln;

	{ ================================= }
	ndongo.id := 1;
	ndongo.prenom := 'Ndongo';
	ndongo.nom := 'Niang';
	writeln('Le representant ' ,ndongo.prenom, ' ' ,ndongo.nom, ' a pour identifiant ' ,ndongo.id);
	writeln;

	{ ================================= }
	compte_ndongo_orange.operateur := 1; // Orange
	compte_ndongo_orange.representant := 1;
	compte_ndongo_orange.solde := 5300;
	writeln('Le compte du representant d''identifiant ' ,compte_ndongo_orange.representant, ' chez l''operateur d''identifiant ' ,
			compte_ndongo_orange.operateur, ' a pour solde ' ,compte_ndongo_orange.solde);
	writeln;

	{ ================================= }
	transfert_modou.id := 1;
	transfert_modou.sorte := S_Types.transfert_argent;
	transfert_modou.compte_source := 1; // Le compte de Modou
	transfert_modou.compte_destination := 1; // Le compte de Ndongo
	transfert_modou.montant := 1000;
	write('L''operation d''identifiant ' ,transfert_modou.id, ' s''elevant a la somme de ' ,transfert_modou.montant, 
			' a comme source le compte d''identifiant ' ,transfert_modou.compte_source, ' et comme destination le compte d''identifiant '
			,transfert_modou.compte_destination, '. Il est de type: ');
	case transfert_modou.sorte of
		S_Types.achat_credit:
			writeln('Achat de credit');
		S_Types.transfert_argent:
			writeln('Transfert d''argent');
		S_Types.retrait_argent:
			writeln('Retrait d''argent');
		S_Types.paiement_facture:
			writeln('Facturation');
		S_Types.creance_representant:
			writeln('Creance representant');
		S_Types.debit_representant:
			writeln('Debit representant');
	end;
	writeln;
end.