program test_client;
uses S_Types, Client;

var modou, niokhor, nabou: S_Types.client;
	idModou, idNiokhor, idNabou: integer;

begin
	modou.prenom := 'Modou';
	modou.nom := 'Ndiaye';
	modou.numero := '771234567';

	niokhor.prenom := 'Niokhor';
	niokhor.nom := 'Gaye';
	niokhor.numero := '771234568';

	nabou.prenom := 'Nabou';
	nabou.nom := 'Sow';
	nabou.numero := '771234569';

	writeln('Insertion de Modou...');
	idModou := Client.inserer(modou);
	writeln('Insertion de Niokhor...');
	idNiokhor := Client.inserer(niokhor);
	writeln('Insertion de Nabou...');
	idNabou := Client.inserer(nabou);
	writeln('Nombre de clients actuel...');
	writeln(Client.compter());

	writeln('Lecture de Modou...');
	modou := Client.lire(idModou);  
	writeln(modou.id);
	writeln(modou.prenom);
	writeln(modou.nom);
	writeln(modou.numero);
		
	writeln('Lecture de Niokhor...');
	niokhor := Client.lire(idNiokhor);
	writeln(niokhor.id);
	writeln(niokhor.prenom);
	writeln(niokhor.nom);
	writeln(niokhor.numero);
		
	writeln('Lecture de Nabou...');
	nabou := Client.lire(idNabou);
	writeln(nabou.id);
	writeln(nabou.prenom);
	writeln(nabou.nom);
	writeln(nabou.numero);

	writeln('Verification de l''existence de l''identifiant ' ,idModou);
	writeln(Client.existe(idModou));
	writeln('Verification de l''existence de l''identifiant ' ,idNiokhor);
	writeln(Client.existe(idNiokhor));
	writeln('Verification de l''existence de l''identifiant ' ,idNabou);
	writeln(Client.existe(idNabou));
	writeln('Verification de l''existence de l''identifiant 1234');
	writeln(Client.existe(1234));

	writeln('Verification de l''existence du numero ' ,modou.numero);
	writeln(Client.numero_existe(modou.numero));
	writeln('Verification de l''existence du numero ' ,niokhor.numero);
	writeln(Client.numero_existe(niokhor.numero));
	writeln('Verification de l''existence du numero ' ,nabou.numero);
	writeln(Client.numero_existe(nabou.numero));
	writeln('Verification de l''existence du numero 779997700');
	writeln(Client.numero_existe('779997700'));

    writeln('Lecture du client avec le numero ' ,modou.numero);
    modou := Client.recherche_numero(modou.numero);
    writeln(modou.id);
    writeln(modou.prenom);
    writeln(modou.nom);
    writeln(modou.numero);

	writeln('Suppression de Niokhor...');
	Client.supprimer(idNiokhor);
	writeln('Nombre de clients actuel...');
	writeln(Client.compter());

	writeln('Nouvelle verification de l''existence de l''identifiant ' ,idNiokhor);
	writeln(Client.existe(idNiokhor));

	writeln('Suppression de tous les clients');
	Client.supprimer(idModou);
	Client.supprimer(idNabou);

end.