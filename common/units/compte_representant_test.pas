program compte_representant_test;

uses CompteRepresentant, S_Types;

var
	compte_saly_wari: S_Types.compte_representant;
	idSalyWari: S_Types.operateur_representant;

begin
	writeln('Insertion du compte de Saly chez Wari...');
	compte_saly_wari.id.operateur := 1;
	compte_saly_wari.id.representant := 1;
	compte_saly_wari.solde := 0;

	idSalyWari := CompteRepresentant.inserer(compte_saly_wari);

	writeln('Verification de l''existence du compte de Saly chez Wari');
	writeln(CompteRepresentant.existe(idSalyWari.operateur, idSalyWari.representant));

	writeln('Lecture du compte de Saly chez Wari...');
	compte_saly_wari := CompteRepresentant.lire(idSalyWari.operateur, idSalyWari.representant);
	writeln(compte_saly_wari.id.operateur);
	writeln(compte_saly_wari.id.representant);
	writeln(compte_saly_wari.solde);

	writeln('Attritbution de 5000f au solde du compte Saly chez Wari');
	CompteRepresentant.determiner_solde(idSalyWari.operateur, idSalyWari.representant, 5000);

	writeln('Relecture du compte de Saly chez Wari');
	compte_saly_wari := CompteRepresentant.lire(idSalyWari.operateur, idSalyWari.representant);
	writeln(compte_saly_wari.id.operateur);
	writeln(compte_saly_wari.id.representant);
	writeln(compte_saly_wari.solde);

	writeln('Suppression du compte de Saly chez Wari');
	CompteRepresentant.supprimer(idSalyWari.operateur, idSalyWari.representant);

	writeln('Reverification de l''existence du compte de Saly chez Wari');
	writeln(CompteRepresentant.existe(idSalyWari.operateur, idSalyWari.representant));
end.