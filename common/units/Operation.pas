unit Operation;

interface
    uses sysutils, S_Types, Constantes;

    { Compte le nombre de operations }
    { @return le nombre de operations }
    function compter(): integer;
    
    { Insere un operation dans le fichier }
    { @param Le operation a inserer }
    { @return l'identifiant du operation insere, sinon -1 }
    function inserer(operation: S_Types.operation): integer;

    { Lit un operation depuis le fichier }
    { @param l'identifiant du operation a lire }
    { @return le operation lu }
    function lire(code: integer): S_Types.operation;

    { Supprime un operation }
    { @param l'identifiant du operation }
    { @return vrai si la suppression a reussi, faux sinon }
    function supprimer(code: integer): boolean;

    { Verifie si le operation identifie par @id existe }
    { @param l'identifiant du operation }
    { @return vrai si le operation existe, faux sinon }
    function existe(code: integer): boolean;

implementation
    
    { ================================= }
    function compter(): integer;
    var
        f_operations: THandle;
    begin
        f_operations := FileOpen(Constantes.fichier_operations, fmOpenRead);
        compter := FileSeek(f_operations, 0, fsFromEnd) div sizeOf(S_Types.operation);
        FileClose(f_operations);
    end;

    { ================================= }
    function ecrire_position(operation: S_Types.operation; index: integer): boolean;
    var
        f_operations: THandle;
        nbOperations: integer;
    begin
        f_operations := FileOpen(Constantes.fichier_operations, fmOpenWrite);
        FileSeek(f_operations, sizeOf(S_Types.operation) * index, fsFromBeginning);
        
        if (FileWrite(f_operations, operation, sizeOf(S_Types.operation)) <> -1) then
            ecrire_position := true
        else
            ecrire_position := false;

        FileClose(f_operations);
    end;

    { ================================= }
    function lire_position(index: integer): S_Types.operation;
    var
        f_operations: THandle;
        operation: S_Types.operation;
    begin
        f_operations := FileOpen(Constantes.fichier_operations, fmOpenRead);
        FileSeek(f_operations, sizeOf(S_Types.operation) * index, fsFromBeginning);
        FileRead(f_operations, operation, sizeOf(S_Types.operation));
        FileClose(f_operations);
        lire_position := operation;
    end;

    { ================================= }
    function chercher_position(code: integer): integer;
    var
        operation: S_Types.operation;
        nbOperations, index: integer; 
    begin
        nbOperations := compter();
        chercher_position := -1;

        for index:=0 to nbOperations - 1 do
        begin
            operation := lire_position(index);
            if (operation.code = code) then
            begin
                chercher_position := index;
                exit;
            end;
        end;
    end;

    { ================================= }
    function inserer(operation: S_Types.operation): integer;
    var
        derniere_operation: S_Types.operation;
        nbOperations: integer;
    begin

        nbOperations := compter();
        if nbOperations <> 0 then
        begin
            derniere_operation := lire_position(nbOperations - 1);
            operation.code := derniere_operation.code + 1;
        end
        else
            operation.code := 1;

        ecrire_position(operation, nbOperations);

        inserer := operation.code;
    end;

    { ================================= }
    function lire(code: integer): S_Types.operation;
    begin
        lire := lire_position(chercher_position(code));
    end;

    { ================================= }
    function supprimer(code: integer): boolean;
    var
        f_operations: THandle;
        operation: S_Types.operation;
        index, nbOperations: integer;
    begin
        nbOperations := compter();
        index := chercher_position(code);

        if (index <> -1) then
        begin
            for index:=index to nbOperations - 2 do
            begin
                operation := lire_position(index + 1);
                ecrire_position(operation, index);
            end;

            f_operations := FileOpen(Constantes.fichier_operations, fmOpenWrite);
            if (f_operations = -1) then
            begin
                supprimer := false;
                exit;
            end;      
            FileTruncate(f_operations, (nbOperations - 1) * sizeOf(S_Types.operation));
            FileClose(f_operations);
            supprimer := true;
        end;

        supprimer := true;
    end;

    { ================================= }
    function existe(code: integer): boolean;
    begin
        if (chercher_position(code) = -1) then
            existe := false
        else
            existe := true;
    end;

end.