unit Representant;

interface
    uses sysutils, S_Types, Constantes;
    
    { Insere un representant dans le fichier }
    { @param Le representant a inserer }
    { @return l'identifiant du representant insere, sinon -1 }
    function inserer(representant: S_Types.representant): integer;

    { Lit un representant depuis le fichier }
    { @param l'identifiant du representant a lire }
    { @return le representant lu }
    function lire(id: integer): S_Types.representant;

    { Supprime un representant }
    { @param l'identifiant du representant }
    { @return vrai si la suppression a reussi, faux sinon }
    function supprimer(id: integer): boolean;

    { Compte le nombre de representants }
    { @return le nombre de representants }
    function compter(): integer;

    { Verifie si le representant identifie par @id existe }
    { @param l'identifiant du representant }
    { @return vrai si le representant existe, faux sinon }
    function existe(id: integer): boolean;

implementation
    
    { ================================= }
    function compter(): integer;
    var
        f_representants: THandle;
    begin
        f_representants := FileOpen(Constantes.fichier_representants, fmOpenRead);
        compter := FileSeek(f_representants, 0, fsFromEnd) div sizeOf(S_Types.representant);
        FileClose(f_representants);
    end;

    { ================================= }
    function ecrire_position(representant: S_Types.representant; index: integer): boolean;
    var
        f_representants: THandle;
        nbClients: integer;
    begin
        f_representants := FileOpen(Constantes.fichier_representants, fmOpenWrite);
        nbClients := compter();
        FileSeek(f_representants, sizeOf(S_Types.representant) * index, fsFromBeginning);
        
        if (FileWrite(f_representants, representant, sizeOf(S_Types.representant)) <> -1) then
            ecrire_position := false
        else
            ecrire_position := true;

        FileClose(f_representants);
    end;

    { ================================= }
    function lire_position(index: integer): S_Types.representant;
    var
        f_representants: THandle;
        representant: S_Types.representant;
    begin
        f_representants := FileOpen(Constantes.fichier_representants, fmOpenRead);
        FileSeek(f_representants, sizeOf(S_Types.representant) * index, fsFromBeginning);
        FileRead(f_representants, representant, sizeOf(S_Types.representant));
        FileClose(f_representants);
        lire_position := representant;
    end;

    { ================================= }
    function chercher_position(id: integer): integer;
    var
        representant: S_Types.representant;
        nbClients, index: integer; 
    begin
        nbClients := compter();
        chercher_position := -1;
        
        for index:=0 to nbClients - 1 do
        begin
            representant := lire_position(index);
            if (representant.id = id) then
            begin
                chercher_position := index;
                exit;
            end;
        end;
    end;

    { ================================= }
    function inserer(representant: S_Types.representant): integer;
    var
        dernier_representant: S_Types.representant;
        nbClients: integer;
    begin

        nbClients := compter();
        if nbClients <> 0 then
        begin
            dernier_representant := lire_position(nbClients - 1);
            representant.id := dernier_representant.id + 1;
        end
        else
            representant.id := 0;

        ecrire_position(representant, nbClients);

        inserer := representant.id;
    end;

    { ================================= }
    function lire(id: integer): S_Types.representant;
    begin
        lire := lire_position(chercher_position(id));
    end;

    { ================================= }
    function supprimer(id: integer): boolean;
    var
        f_representants: THandle;
        representant: S_Types.representant;
        index, nbClients: integer;
    begin
        nbClients := compter();
        index := chercher_position(id);

        if (index <> -1) then
        begin
            for index:=index to nbClients - 2 do
            begin
                representant := lire_position(index + 1);
                ecrire_position(representant, index);
            end;

            f_representants := FileOpen(Constantes.fichier_representants, fmOpenWrite);
            FileTruncate(f_representants, (nbClients - 1) * sizeOf(S_Types.representant));
            FileClose(f_representants);
        end;

        supprimer := true;
    end;

    { ================================= }
    function existe(id: integer): boolean;
    var
        representant: S_Types.representant;
        index, nbClients: integer;
        tmp_existe: boolean;
    begin
        nbClients := compter();

        tmp_existe := false;
        for index:=0 to nbClients - 1 do
        begin
            representant := lire_position(index);
            if (representant.id = id) then
            begin
                tmp_existe := true;
                break;
            end;
        end;

        existe := tmp_existe;
    end;


end.