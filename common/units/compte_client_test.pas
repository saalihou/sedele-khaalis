program compte_client_test;

uses CompteClient, S_Types;

var
	compte_modou: S_Types.compte_client;
	idModou: integer;

begin
	writeln('Insertion du compte de Modou...');
	compte_modou.proprietaire := 1;
	compte_modou.solde := 0;
	compte_modou.credit_tel := 0;

	idModou := CompteClient.inserer(compte_modou);

	writeln('Verification de l''existence du compte de Modou');
	writeln(CompteClient.existe(idModou));

	writeln('Lecture du compte de Modou...');
	compte_modou := CompteClient.lire(idModou);
	writeln(compte_modou.proprietaire);
	writeln(compte_modou.solde);
	writeln(compte_modou.credit_tel);

	writeln('Attritbution de 5000f au solde du compte Modou');
	CompteClient.determiner_solde(idModou, 5000);

	writeln('Attritbution de 1000f au credit telephonique de Modou');
	CompteClient.determiner_credit(idModou, 1000);

	writeln('Relecture du compte de Modou');
	compte_modou := CompteClient.lire(idModou);
	writeln(compte_modou.proprietaire);
	writeln(compte_modou.solde);
	writeln(compte_modou.credit_tel);

	writeln('Suppression du compte de Modou');
	CompteClient.supprimer(idModou);

	writeln('Reverification de l''existence du compte de Modou');
	writeln(CompteClient.existe(idModou));

end.