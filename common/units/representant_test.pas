program test_representant;
uses S_Types, Representant;

var samba, marie: S_Types.representant;
	idSamba, idMarie: integer;

begin
	samba.prenom := 'Samba';
	samba.nom := 'Ndiaye';

	marie.prenom := 'Marie';
	marie.nom := 'Ndiago';

	writeln('Insertion de Samba...');
	idSamba := Representant.inserer(samba);
	writeln('Insertion de Marie...');
	idMarie := Representant.inserer(marie);
	writeln('Nombre de representants actuel...');
	writeln(Representant.compter());

	writeln('Lecture de Samba...');
	samba := Representant.lire(idSamba);	
	writeln(samba.id);
	writeln(samba.prenom);
	writeln(samba.nom);

	writeln('Lecture de Marie...');
	marie := Representant.lire(idMarie);	
	writeln(marie.id);
	writeln(marie.prenom);
	writeln(marie.nom);

	writeln('Verification de l''existence de l''identifiant ' ,idSamba);
	writeln(Representant.existe(idSamba));
	writeln('Verification de l''existence de l''identifiant ' ,idMarie);
	writeln(Representant.existe(idMarie));
	writeln('Verification de l''existence de l''identifiant 1234');
	writeln(Representant.existe(1234));

	writeln('Suppression de Samba...');
	Representant.supprimer(idSamba);
	writeln('Nombre de representants actuel...');
	writeln(Representant.compter());

	writeln('Nouvelle verification de l''existence de l''identifiant ' ,idSamba);
	writeln(Representant.existe(idSamba));

	writeln('Suppression de tous les representants');
	Representant.supprimer(idSamba);
	Representant.supprimer(idMarie);

end.