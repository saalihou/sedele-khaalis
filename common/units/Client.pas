unit Client;

interface
    uses sysutils, S_Types, Constantes;

    { Compte le nombre de clients }
    { @return le nombre de clients }
    function compter(): integer;
    
    { Insere un client dans le fichier }
    { @param Le client a inserer }
    { @return l'identifiant du client insere, sinon -1 }
    function inserer(client: S_Types.client): integer;

    { Lit un client depuis le fichier }
    { @param l'identifiant du client a lire }
    { @return le client lu }
    function lire(id: integer): S_Types.client;

    { Supprime un client }
    { @param l'identifiant du client }
    { @return vrai si la suppression a reussi, faux sinon }
    function supprimer(id: integer): boolean;

    { Retourne le client avec le numero @numero }
    { @param le numero du client a rechercher }
    { @return le client trouve }
    function recherche_numero(numero: string): S_Types.client;

    { Verifie si le client identifie par @id existe }
    { @param l'identifiant du client }
    { @return vrai si le client existe, faux sinon }
    function existe(id: integer): boolean;

    { Verifie si le numero du client existe }
    { @param le numero du client }
    { @return vrai si le client existe, faux sinon }
    function numero_existe(numero: string): boolean;

implementation
    
    { ================================= }
    function compter(): integer;
    var
        f_clients: THandle;
    begin
        f_clients := FileOpen(Constantes.fichier_clients, fmOpenRead);
        compter := FileSeek(f_clients, 0, fsFromEnd) div sizeOf(S_Types.client);
        FileClose(f_clients);
    end;

    { ================================= }
    function ecrire_position(client: S_Types.client; index: integer): boolean;
    var
        f_clients: THandle;
        nbClients: integer;
    begin
        f_clients := FileOpen(Constantes.fichier_clients, fmOpenWrite);
        FileSeek(f_clients, sizeOf(S_Types.client) * index, fsFromBeginning);
        
        if (FileWrite(f_clients, client, sizeOf(S_Types.client)) <> -1) then
            ecrire_position := false
        else
            ecrire_position := true;

        FileClose(f_clients);
    end;

    { ================================= }
    function lire_position(index: integer): S_Types.client;
    var
        f_clients: THandle;
        client: S_Types.client;
    begin
        f_clients := FileOpen(Constantes.fichier_clients, fmOpenRead);
        FileSeek(f_clients, sizeOf(S_Types.client) * index, fsFromBeginning);
        FileRead(f_clients, client, sizeOf(S_Types.client));
        FileClose(f_clients);
        lire_position := client;
    end;

    { ================================= }
    function chercher_position(id: integer): integer;
    var
        client: S_Types.client;
        nbClients, index: integer; 
    begin
        nbClients := compter();
        chercher_position := -1;

        for index:=0 to nbClients - 1 do
        begin
            client := lire_position(index);
            if (client.id = id) then
            begin
                chercher_position := index;
                exit;
            end;
        end;
    end;

    { ================================= }
    function chercher_position_numero(numero: string): integer;
    var
        client: S_Types.client;
        nbClients, index: integer; 
    begin
        nbClients := compter();
        chercher_position_numero := -1;

        for index:=0 to nbClients - 1 do
        begin
            client := lire_position(index);
            if (client.numero = numero) then
            begin
                chercher_position_numero := index;
                exit;
            end;
        end;
    end;

    { ================================= }
    function inserer(client: S_Types.client): integer;
    var
        dernier_client: S_Types.client;
        nbClients: integer;
    begin

        nbClients := compter();
        if nbClients <> 0 then
        begin
            dernier_client := lire_position(nbClients - 1);
            client.id := dernier_client.id + 1;
        end
        else
            client.id := 1;

        ecrire_position(client, nbClients);

        inserer := client.id;
    end;

    { ================================= }
    function lire(id: integer): S_Types.client;
    begin
        lire := lire_position(chercher_position(id));
    end;

    { ================================= }
    function supprimer(id: integer): boolean;
    var
        f_clients: THandle;
        client: S_Types.client;
        index, nbClients: integer;
    begin
        nbClients := compter();
        index := chercher_position(id);

        if (index <> -1) then
        begin
            for index:=index to nbClients - 2 do
            begin
                client := lire_position(index + 1);
                ecrire_position(client, index);
            end;

            f_clients := FileOpen(Constantes.fichier_clients, fmOpenWrite);
            if (f_clients = -1) then
            begin
                supprimer := false;
                exit;
            end;      
            FileTruncate(f_clients, (nbClients - 1) * sizeOf(S_Types.client));
            FileClose(f_clients);
            supprimer := true;
        end;

        supprimer := true;
    end;

    { ================================= }
    function recherche_numero(numero: string): S_Types.client;
    var
        position: integer;
    begin
        position := chercher_position_numero(numero);
        if (position <> -1) then
            recherche_numero := lire_position(position);
    end;

    { ================================= }
    function existe(id: integer): boolean;
    begin
        if (chercher_position(id) = -1) then
            existe := false
        else
            existe := true;
    end;

    { ================================= }
    function numero_existe(numero: string): boolean;
    var
        client: S_Types.client;
        index, nbClients: integer;
        tmp_existe: boolean;
    begin

        nbClients := compter();

        tmp_existe := false;
        for index:=0 to nbClients - 1 do
        begin
            client := lire_position(index);
            if (client.numero = numero) then
            begin
                tmp_existe := true;
                break;
            end;
        end;

        numero_existe := tmp_existe;
    end;


end.