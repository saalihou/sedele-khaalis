unit Operateur;

interface
    uses sysutils, S_Types, Constantes;
    
    { Insere un opérateur dans le fichier }
    { @param L'opérateur a inserer }
    { @return l'identifiant de l'opérateur insere, sinon -1 }
    function inserer(operateur: S_Types.operateur): integer;

    { Lit un operateur depuis le fichier }
    { @param l'identifiant de l'operateur a lire }
    { @return l'operateur lu }
    function lire(id: integer): S_Types.operateur;

    { Supprime un operateur }
    { @param l'identifiant de l'operateur }
    { @return vrai si la suppression a reussi, faux sinon }
    function supprimer(id: integer): boolean;

    { Compte le nombre de operateurs }
    { @return le nombre de operateurs }
    function compter(): integer;

    { Verifie si le operateur identifie par @id existe }
    { @param l'identifiant du operateur }
    { @return vrai si le operateur existe, faux sinon }
    function existe(id: integer): boolean;

    { Liste les serives de l'opérateur }
    { @param le nom de la variable opérateur }
    { @return 0 c'est une procédure }
    procedure liste_services(operateur: S_Types.operateur);

    { Recherche un opérateur }
    { @param le nom de la variable opérateur }
    { @return l'id de l'opérateur }
    function recherche_operateur(operateur: S_Types.operateur):integer;
    
    { Liste les services de l'opérateur }
    { @param le nom de la variable opérateur }
    { @return 0 c'est une procédure }
    function liste_operateurs():S_Types.tableau_operateurs;


implementation
    
    { ================================= }
    function compter(): integer;
    var
        f_operateurs: THandle;
    begin
        f_operateurs := FileOpen(Constantes.fichier_operateurs, fmOpenRead);
        compter := FileSeek(f_operateurs, 0, fsFromEnd) div sizeOf(S_Types.operateur);
        FileClose(f_operateurs);
    end;

    { ================================= }
    function ecrire_position(operateur: S_Types.operateur; index: integer): boolean;
    var
        f_operateurs: THandle;
        nboperateurs: integer;
    begin
        f_operateurs := FileOpen(Constantes.fichier_operateurs, fmOpenWrite);
        nboperateurs := compter();
        FileSeek(f_operateurs, sizeOf(S_Types.operateur) * index, fsFromBeginning);
        
        if (FileWrite(f_operateurs, operateur, sizeOf(S_Types.operateur)) <> -1) then
            ecrire_position := false
        else
            ecrire_position := true;

        FileClose(f_operateurs);
    end;

    { ================================= }
    function lire_position(index: integer): S_Types.operateur;
    var
        f_operateurs: THandle;
        operateur: S_Types.operateur;
    begin
        f_operateurs := FileOpen(Constantes.fichier_operateurs, fmOpenRead);
        FileSeek(f_operateurs, sizeOf(S_Types.operateur) * index, fsFromBeginning);
        FileRead(f_operateurs, operateur, sizeOf(S_Types.operateur));
        FileClose(f_operateurs);
        lire_position := operateur;
    end;

    { ================================= }
    function chercher_position(id: integer): integer;
    var
        operateur: S_Types.operateur;
        nboperateurs, index: integer; 
    begin
        nboperateurs := compter();

        for index:=0 to nboperateurs - 1 do
        begin
            operateur := lire_position(index);
            if (operateur.id = id) then
            begin
                chercher_position := index;
                exit;
            end
            else
            begin
                if (index = nboperateurs - 1) then
                begin
                    chercher_position := -1;
                    exit;
                end;
            end;
        end;
    end;

    { ================================= }
    function inserer(operateur: S_Types.operateur): integer;
    var
        dernier_operateur: S_Types.operateur;
        nboperateurs: integer;
    begin

        nboperateurs := compter();
        if nboperateurs <> 0 then
        begin
            dernier_operateur := lire_position(nboperateurs - 1);
            operateur.id := dernier_operateur.id + 1;
        end
        else
            operateur.id := 0;

        ecrire_position(operateur, nboperateurs);

        inserer := operateur.id;
    end;

    { ================================= }
    function lire(id: integer): S_Types.operateur;
    begin
        lire := lire_position(chercher_position(id));
    end;

    { ================================= }
    function supprimer(id: integer): boolean;
    var
        f_operateurs: THandle;
        operateur: S_Types.operateur;
        index, nboperateurs: integer;
    begin
        nboperateurs := compter();
        index := chercher_position(id);

        if (index <> -1) then
        begin
            for index:=index to nboperateurs - 2 do
            begin
                operateur := lire_position(index + 1);
                ecrire_position(operateur, index);
            end;

            f_operateurs := FileOpen(Constantes.fichier_operateurs, fmOpenWrite);
            FileTruncate(f_operateurs, (nboperateurs - 1) * sizeOf(S_Types.operateur));
            FileClose(f_operateurs);
        end;

        supprimer := true;
    end;

    { ================================= }
    function existe(id: integer): boolean;
    var
        operateur: S_Types.operateur;
        index, nboperateurs: integer;
        tmp_existe: boolean;
    begin
        nboperateurs := compter();

        tmp_existe := false;
        for index:=0 to nboperateurs - 1 do
        begin
            operateur := lire_position(index);
            if (operateur.id = id) then
            begin
                tmp_existe := true;
                break;
            end;
        end;

        existe := tmp_existe;
    end;

    { ================================= }
    procedure liste_services(operateur: S_Types.operateur);
    var
        i:integer;
    begin
        for i:=0 to operateur.nbreServices-1 do
        begin
            case operateur.services[i] of
                S_Types.credit: writeln('Crédit');
                S_Types.facture: writeln('facture');
                S_Types.retrait: writeln('Retrait');
                S_Types.transfert: writeln('Transfert');
            end;
        end;
    end;

    { ================================= }
    function liste_operateurs():S_Types.tableau_operateurs;
    var
        i:integer;
    begin
        for i:=0 to compter()-1 do
            liste_operateurs[i]:=lire_position(i);
    end;

    { ================================= }
    function recherche_operateur(operateur: S_Types.operateur):integer;
    begin
        if existe(operateur.id)=true then
            recherche_operateur:=operateur.id
        else
            recherche_operateur:=-1;
    end;


end.