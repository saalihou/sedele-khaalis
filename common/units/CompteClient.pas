unit CompteClient;

interface
    uses sysutils, S_Types, Constantes, Client;
    
    { Insere un compte de client dans le fichier }
    { @param Le compte de client a inserer }
    { @return le proprietaire du compte insere, sinon -1 }
    function inserer(compte_client: S_Types.compte_client): integer;

    { Lit un compte de client depuis un fichier }
    { @param l'identifiant du compte de client a lire }
    { @return le compte de client lu }
    function lire(proprietaire: integer): S_Types.compte_client;

    { Supprime un compte de client }
    { @param l'identifiant du proprietaire du compte }
    { @return vrai si la suppression a reussi, faux sinon }
    function supprimer(proprietaire: integer): boolean;

    { Solde un compte de client }
    { @param le proprietaire du compte }
    { @param le montant a attribuer au solde du compte }    
    { @return vrai si le solde a ete attribue, faux sinon }
    function determiner_solde(proprietaire: integer; montant: longint): boolean;

    { Determine le credit telephonique d'un compte de client }
    { @param le proprietaire du compte }
    { @param le montant a attribuer au credit telephonique du compte }    
    { @return vrai si le credit a ete attribue, faux sinon }
    function determiner_credit(proprietaire, montant: integer): boolean;

    { Verifie si le compte du client appartenant a @proprietaire existe }
    { @param le proprietaire du compte }
    { @return vrai si le compte du client existe, faux sinon }
    function existe(proprietaire: integer): boolean;

implementation
    
    { ================================= }
    function compter(): integer;
    var
        f_comptes_clients: THandle;
    begin
        f_comptes_clients := FileOpen(Constantes.fichier_comptes_clients, fmOpenRead);
        compter := FileSeek(f_comptes_clients, 0, fsFromEnd) div sizeOf(S_Types.compte_client);
        FileClose(f_comptes_clients);
    end;

    { ================================= }
    function ecrire_position(compte_client: S_Types.compte_client; index: integer): boolean;
    var
        f_comptes_clients: THandle;
        nbComptes: integer;
    begin
        f_comptes_clients := FileOpen(Constantes.fichier_comptes_clients, fmOpenWrite);
        nbComptes := compter();
        FileSeek(f_comptes_clients, sizeOf(S_Types.compte_client) * index, fsFromBeginning);
        
        if (FileWrite(f_comptes_clients, compte_client, sizeOf(S_Types.compte_client)) <> -1) then
            ecrire_position := false
        else
            ecrire_position := true;

        FileClose(f_comptes_clients);
    end;

    { ================================= }
    function lire_position(index: integer): S_Types.compte_client;
    var
        f_comptes_clients: THandle;
        compte_client: S_Types.compte_client;
        nbComptes: integer;
    begin
        f_comptes_clients := FileOpen(Constantes.fichier_comptes_clients, fmOpenRead);
        nbComptes := compter();
        FileSeek(f_comptes_clients, sizeOf(S_Types.compte_client) * index, fsFromBeginning);
        FileRead(f_comptes_clients, compte_client, sizeOf(S_Types.compte_client));
        lire_position := compte_client;
        FileClose(f_comptes_clients);
    end;

    { ================================= }
    function chercher_position(proprietaire: integer): integer;
    var
        compte_client: S_Types.compte_client;
        nbComptes, index: integer; 
    begin
        nbComptes := compter();
        chercher_position := -1;

        for index:=0 to nbComptes - 1 do
        begin
            compte_client := lire_position(index);
            if (compte_client.proprietaire = proprietaire) then
            begin
                chercher_position := index;
                exit;
            end;
        end;
    end;
    
    { ================================= }
    function inserer(compte_client: S_Types.compte_client): integer;
    var
        dernier_compte_client : S_Types.compte_client;
        nbComptes: integer;
    begin
        nbComptes := compter();
        ecrire_position(compte_client, nbComptes);
        inserer := compte_client.proprietaire;
    end;

    { ================================= }
    function lire(proprietaire: integer): S_Types.compte_client;
    begin
        lire := lire_position(chercher_position(proprietaire));
    end;

    { ================================= }
    function supprimer(proprietaire: integer): boolean;
    var
        f_comptes_clients: THandle;
        compte_client: S_Types.compte_client;
        index, nbComptes: integer;
    begin
        nbComptes := compter();
        index := chercher_position(proprietaire);

        if (index <> - 1) then
        begin
            for index:=index to nbComptes - 2 do
            begin
                compte_client := lire_position(index + 1);
                ecrire_position(compte_client, index);
            end;

            f_comptes_clients := FileOpen(Constantes.fichier_comptes_clients, fmOpenWrite);
            FileTruncate(f_comptes_clients, (nbComptes - 1) * sizeOf(S_Types.compte_client));
            FileClose(f_comptes_clients);
        end;

        supprimer := true;
    end;

    { ================================= }
    function determiner_solde(proprietaire: integer; montant: longint): boolean;
    var
        compte_client: S_Types.compte_client;
        index: integer;
    begin
        index := chercher_position(proprietaire);
        compte_client := lire_position(index);
        compte_client.solde := montant;
        ecrire_position(compte_client, index);
        determiner_solde := true;
    end;

    { ================================= }
    function determiner_credit(proprietaire, montant: integer): boolean;
    var
        compte_client: S_Types.compte_client;
        index: integer;
    begin
        index := chercher_position(proprietaire);
        compte_client := lire_position(index);
        compte_client.credit_tel := montant;
        ecrire_position(compte_client, index);
        determiner_credit := true;
    end;

    { ================================= }
    function existe(proprietaire: integer): boolean;
    begin
        if (chercher_position(proprietaire) = -1) then
            existe := false
        else
            existe := true;
    end;
end.