unit CompteOperateur;

interface
    uses sysutils, S_Types, Constantes, Operateur;
    
    { Insere un compte de Operateur dans le fichier }
    { @param Le compte de Operateur a inserer }
    { @return le proprietaire du compte insere, sinon -1 }
    function inserer(compte_operateur: S_Types.compte_operateur): integer;

    { Lit un compte de Operateur depuis un fichier }
    { @param l'identifiant du compte de Operateur a lire }
    { @return le compte de Operateur lu }
    function lire(proprietaire: integer): S_Types.compte_operateur;

    { Solde un compte de Operateur }
    { @param le proprietaire du compte }
    { @param le montant a attribuer au solde du compte }    
    { @return vrai si le solde a ete attribue, faux sinon }
    function determiner_solde(proprietaire: integer; montant: longint): boolean;

    { Supprime un compte de Operateur }
    { @param l'identifiant du proprietaire du compte }
    { @return vrai si la suppression a reussi, faux sinon }
    function supprimer(proprietaire: integer): boolean;

    { Verifie si le compte du Operateur appartenant a @proprietaire existe }
    { @param le proprietaire du compte }
    { @return vrai si le compte du Operateur existe, faux sinon }
    function existe(proprietaire: integer): boolean;

implementation
    
    { ================================= }
    function compter(): integer;
    var
        f_comptes_Operateurs: THandle;
    begin
        f_comptes_Operateurs := FileOpen(Constantes.fichier_comptes_Operateurs, fmOpenRead);
        compter := FileSeek(f_comptes_Operateurs, 0, fsFromEnd) div sizeOf(S_Types.compte_operateur);
        FileClose(f_comptes_Operateurs);
    end;

    { ================================= }
    function ecrire_position(compte_operateur: S_Types.compte_operateur; index: integer): boolean;
    var
        f_comptes_Operateurs: THandle;
        nbComptes: integer;
    begin
        f_comptes_Operateurs := FileOpen(Constantes.fichier_comptes_Operateurs, fmOpenWrite);
        nbComptes := compter();
        FileSeek(f_comptes_Operateurs, sizeOf(S_Types.compte_operateur) * index, fsFromBeginning);
        
        if (FileWrite(f_comptes_Operateurs, compte_operateur, sizeOf(S_Types.compte_operateur)) <> -1) then
            ecrire_position := false
        else
            ecrire_position := true;

        FileClose(f_comptes_Operateurs);
    end;

    { ================================= }
    function lire_position(index: integer): S_Types.compte_operateur;
    var
        f_comptes_Operateurs: THandle;
        compte_operateur: S_Types.compte_operateur;
        nbComptes: integer;
    begin
        f_comptes_Operateurs := FileOpen(Constantes.fichier_comptes_Operateurs, fmOpenRead);
        nbComptes := compter();
        FileSeek(f_comptes_Operateurs, sizeOf(S_Types.compte_operateur) * index, fsFromBeginning);
        FileRead(f_comptes_Operateurs, compte_operateur, sizeOf(S_Types.compte_operateur));
        lire_position := compte_operateur;
        FileClose(f_comptes_Operateurs);
    end;

    { ================================= }
    function chercher_position(proprietaire: integer): integer;
    var
        compte_operateur: S_Types.compte_operateur;
        nbComptes, index: integer; 
    begin
        nbComptes := compter();
        chercher_position := -1;

        for index:=0 to nbComptes - 1 do
        begin
            compte_operateur := lire_position(index);
            if (compte_operateur.proprietaire = proprietaire) then
            begin
                chercher_position := index;
                exit;
            end;
        end;
    end;
    
    { ================================= }
    function inserer(compte_operateur: S_Types.compte_operateur): integer;
    var
        dernier_compte_operateur : S_Types.compte_operateur;
        nbComptes: integer;
    begin
        nbComptes := compter();
        ecrire_position(compte_operateur, nbComptes);
        inserer := compte_operateur.proprietaire;
    end;

    { ================================= }
    function lire(proprietaire: integer): S_Types.compte_operateur;
    begin
        lire := lire_position(chercher_position(proprietaire));
    end;

    { ================================= }
    function supprimer(proprietaire: integer): boolean;
    var
        f_comptes_Operateurs: THandle;
        compte_operateur: S_Types.compte_operateur;
        index, nbComptes: integer;
    begin
        nbComptes := compter();
        index := chercher_position(proprietaire);

        if (index <> - 1) then
        begin
            for index:=index to nbComptes - 2 do
            begin
                compte_operateur := lire_position(index + 1);
                ecrire_position(compte_operateur, index);
            end;

            f_comptes_Operateurs := FileOpen(Constantes.fichier_comptes_Operateurs, fmOpenWrite);
            FileTruncate(f_comptes_Operateurs, (nbComptes - 1) * sizeOf(S_Types.compte_operateur));
            FileClose(f_comptes_Operateurs);
        end;

        supprimer := true;
    end;

    { ================================= }
    function determiner_solde(proprietaire: integer; montant: longint): boolean;
    var
        compte_operateur: S_Types.compte_operateur;
        index: integer;
    begin
        index := chercher_position(proprietaire);
        compte_operateur := lire_position(index);
        compte_operateur.solde := montant;
        ecrire_position(compte_operateur, index);
        determiner_solde := true;
    end;

    { ================================= }
    function existe(proprietaire: integer): boolean;
    begin
        if (chercher_position(proprietaire) = -1) then
            existe := false
        else
            existe := true;
    end;
end.