program test_operateur;
uses S_Types, Operateur;

var OrangeMoney, Wari, Tigo: S_Types.operateur;
	idOrangeMoney, idWari, idTigo, i : integer;
	tableau_operateurs:array[0..999] of S_Types.operateur;

begin
	OrangeMoney.nom := 'OrangeMoney';
	OrangeMoney.nbreServices := 2;
	OrangeMoney.services[0]:=facture;
	OrangeMoney.services[1]:=credit;

	Wari.nom := 'Wari';
	Wari.nbreServices := 2;
	Wari.services[0]:=retrait;
	Wari.services[1]:=transfert;

	Tigo.nom := 'Tigo';
	Tigo.nbreServices := 1;
	Tigo.services[0]:=credit;

	writeln('Insertion de OrangeMoney...');
	idOrangeMoney := operateur.inserer(OrangeMoney);
	writeln('Insertion de Wari...');
	idWari := operateur.inserer(Wari);
	writeln('Insertion de Tigo...');
	idTigo := operateur.inserer(Tigo);
	writeln('Nombre de operateurs actuel...');
	writeln(operateur.compter());

	writeln('Lecture de OrangeMoney...');
	OrangeMoney := operateur.lire(idOrangeMoney);	
	writeln(OrangeMoney.id);
	writeln(OrangeMoney.nom);
	writeln(OrangeMoney.nbreServices,' service(s)');
	liste_services(OrangeMoney);
		
	writeln('Lecture de Wari...');
	Wari := operateur.lire(idWari);
	writeln(Wari.id);
	writeln(Wari.nom);
	writeln(Wari.nbreServices,' service(s)');
	liste_services(Wari);	
		
	writeln('Lecture de Tigo...');
	Tigo := operateur.lire(idTigo);
	writeln(Tigo.id);	
	writeln(Tigo.nom);
	writeln(Tigo.nbreServices,' service(s)');
	liste_services(Tigo);	

	writeln('Verification de l''existence de l''identifiant ' ,idOrangeMoney);
	writeln(operateur.existe(idOrangeMoney));
	writeln('idOrangeMOney=',recherche_operateur(OrangeMoney));
	writeln('Verification de l''existence de l''identifiant ' ,idWari);
	writeln(operateur.existe(idWari));
	writeln('idWari=',recherche_operateur(Wari));
	writeln('Verification de l''existence de l''identifiant ' ,idTigo);
	writeln(operateur.existe(idTigo));
	writeln('idTigo=',recherche_operateur(Tigo));
	writeln('Verification de l''existence de l''identifiant 1234');
	writeln(operateur.existe(1234));

	writeln('Liste des opérateurs');
	for i:=0 to operateur.compter()-1 do
	begin
		tableau_operateurs[i]:=liste_operateurs[i];
		writeln(tableau_operateurs[i].nom);
	end;

	writeln('Suppression de Wari...');
	operateur.supprimer(idWari);
	writeln('Nombre de operateurs actuel...');
	writeln(operateur.compter());
	writeln('Liste des opérateurs');
	for i:=0 to operateur.compter()-1 do
	begin
		tableau_operateurs[i]:=liste_operateurs[i];
		writeln(tableau_operateurs[i].nom);
	end;

	writeln('Nouvelle verification de l''existence de l''identifiant ' ,idWari);
	writeln(operateur.existe(idWari));
	writeln('idWari=',recherche_operateur(Wari));

	writeln('Suppression de tous les operateurs');
	operateur.supprimer(idOrangeMoney);
	operateur.supprimer(idTigo);
	writeln('Liste des opérateurs');
	for i:=0 to operateur.compter()-1 do
	begin
		tableau_operateurs[i]:=liste_operateurs[i];
		writeln(tableau_operateurs[i].nom);
	end;

end.