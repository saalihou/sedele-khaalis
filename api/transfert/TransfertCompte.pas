unit TransfertCompte;

interface
    uses sysutils, Types, Constantes, Operation;

    type liste_operations = array[0..999] of Types.operations;
    
    function chercher_operations_transfert_client(id: integer): liste_operations;
    function chercher_operations_transfert_operateur(id: integer): liste_operations;

implementation


end.